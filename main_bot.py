#!/usr/bin/env python
# -*- coding: utf-8 -*-

from flask import Flask, request
from config import DEBUG, THANKS_PHOTO, MODERATOR_ACTIVE
from config_server import TOKEN, HOST, PORT, CERT, CERT_KEY
from utils import getdb, savedb, checkdb
import telegram
import time
import logging
import traceback
from client import Client

logging.basicConfig(filename='bot.log', format='%(asctime)s %(levelname)-8s %(message)s', level=logging.INFO,
                    datefmt='%Y-%m-%d %H:%M:%S')

bot = telegram.Bot(TOKEN)
app = Flask(__name__)
context = (CERT, CERT_KEY)


def get_type_msg(update):
    if update.update_id is not None and update.callback_query is not None:
        return 'callback_query', update.callback_query.data
    elif update.message is not None:
        if update.message.contact is not None and update.message.contact.phone_number is not None:
            return 'phone', update.message.contact.phone_number
        if update.message.entities is not None and len(update.message.entities) > 0:
            entity = update.message.entities[0]
            if entity.type == 'bot_command':
                return 'bot_command', update.message.text
        if update.message.chat_id is not None:
            return 'simple', update.message.text


def update_msg(update):
    try:
        query = update.callback_query
        chat_id = query.message.chat_id

        # print 'data= ', query.data
        client = Client(chat_id)
        if client.is_end:
            return True
        prev_type = client.get_current_type
        client.go(query.data)

        menu = client.menu(bot=bot, msg_id=chat_id)
        print 'client_current', client.current, menu
        if menu[0] == 'menu':
            if prev_type == 'menu' and client.isfull_and_check:
                bot.send_message(text=menu[1], chat_id=chat_id, reply_markup=menu[2])
            else:
                bot.edit_message_text(text=menu[1], chat_id=chat_id,
                                      message_id=query.message.message_id,
                                      reply_markup=menu[2])

        elif menu[0] in ['text', 'photo']:
            if client.is_end:
                bot.delete_message(chat_id=chat_id, message_id=query.message.message_id)
                bot.send_photo(chat_id=chat_id, photo=open(THANKS_PHOTO, 'rb'), message_id=query.message.message_id)
                if type(menu[1]) is list:
                    for t in menu[1]:
                        bot.send_message(text=t,
                                         chat_id=chat_id)
                else:
                    bot.send_message(text=menu[1], chat_id=chat_id)
            else:
                bot.edit_message_text(text=menu[1],
                                      chat_id=chat_id,
                                      message_id=query.message.message_id)

        if client.is_end and MODERATOR_ACTIVE:
            db = getdb()
            print 'db = {}'.format(db)
            if checkdb(db):
                bot.send_message(text=client.get_info, chat_id=db['moderator'])
                bot.send_photo(chat_id=db['moderator'], photo=client.photo)

        return True
    except:
        print logging.error(traceback.print_exc())
        if DEBUG:
            traceback.print_exc()
        logging.error(traceback.print_exc())
    return False


@app.route('/')
def index():
    return 'index'


def start(bot, update):
    chat_id = update.message.chat_id
    print 'start', update.de_json()
    client = Client(chat_id, is_new=True, username=update.message.from_user.username)
    print 'username', update.message.from_user.username
    m, text, reply_markup = client.menu()

    bot.send_message(text=text, chat_id=chat_id, reply_markup=reply_markup)


@app.route('/' + TOKEN, methods=['POST'])
def webhook():
    try:
        response = request.get_json(force=True)
        print response
        logging.info(response)
        update = telegram.update.Update.de_json(response, bot)

        msg_type = get_type_msg(update)
        print msg_type

        # update message
        if msg_type[0] == 'callback_query':
            upd = update_msg(update)
            if upd:
                return 'ok'
            elif upd is None:
                return 'error'

        is_bot_start = msg_type[0] == 'bot_command' and msg_type[1] == '/start'
        is_moderator = msg_type[0] == 'bot_command' and msg_type[1] == '/moderator'

        if DEBUG:
            is_test = msg_type[0] == 'bot_command' and msg_type[1] == '/test'
            if is_test:
                contact_keyboard = telegram.KeyboardButton(text="send_contact", request_contact=True)
                custom_keyboard = [[contact_keyboard]]
                reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard, one_time_keyboard=True,
                                                            resize_keyboard=True)
                bot.send_message(chat_id=update.message.chat_id, text="Would you mind sharing your location and contact with me?",
                                 reply_markup=reply_markup)
                return 'ok'

        if is_moderator:
            db = getdb()
            print 'db = {}'.format(db)
            db['moderator'] = update.message.chat_id
            savedb(db)
            bot.sendMessage(chat_id=update.message.chat_id, text='Moderator has installed')
            logging.info('bot.sendMessage(chat_id={0}, text=Moderator has installed)'.format(update.message.chat_id))
            return 'ok'

        is_new = False
        if is_bot_start:
            is_new = True
        client = Client(update.message.chat_id, is_new=is_new, username=update.message.from_user.username)

        if msg_type[0] == 'phone':
            client.go(msg_type[1])
            chat_id = update.update_id
            menu = client.menu(bot=bot, msg_id=client.id)
            print 'photo_menu', client.phone

            if menu[0] == 'menu':
                bot.send_message(text=menu[1], chat_id=client.id, reply_markup=menu[2])
            elif menu[0] in ['text', 'photo']:
                bot.sendMessage(text=menu[1],
                                chat_id=client.id,
                                # message_id=update.message.message_id
                                )
            return 'ok'

        if client.is_end:
            return 'end'

        # Send question
        if (msg_type[0] == 'simple' and client.get_current_type != 'menu' and client.current != 'phone') or is_bot_start:
            chat_id = update.message.chat_id
            if is_bot_start:
                # bot.send_photo(chat_id=update.message.chat_id, photo=client.photo)
                print 'username', update.message.from_user.username
                m, text, reply_markup = client.menu()

                bot.send_message(text=text, chat_id=chat_id, reply_markup=reply_markup)
                return 'ok'
            else:
                if client.current == 'photo':
                    res = client.save_photo(update.message)
                    if res is None:
                        return 'need photo'
                    # bot.send_photo(chat_id=update.message.chat_id, photo=open(client.photo, 'rb'))

                client.go(update.message.text)
                menu = client.menu(bot=bot, msg_id=chat_id)
                print 'client_current', client.current, menu
                if menu[0] == 'menu':
                    bot.send_message(text=menu[1], chat_id=chat_id, reply_markup=menu[2])
                elif menu[0] in ['text', 'photo']:
                    bot.sendMessage(text=menu[1],
                                  chat_id=chat_id,
                                  # message_id=update.message.message_id
                                    )

        elif client.current == 'phone':
            print 'phote request twice'
            # bot.sendMessage(text=client.current_question,  chat_id=client.id, one_time_keyboard=True)
            client.send_request_contact(bot, client.current_question, client.id)

    except Exception:
        print traceback.print_exc()
        return 'error'
    return 'ok'


def setwebhook():
    bot.setWebhook(url='https://%s:%s/%s' % (HOST, PORT, TOKEN),
                   certificate=open(CERT, 'rb'))


if __name__ == '__main__':
    setwebhook()
    time.sleep(5)
    print 'Start Flask'
    try:
        app.run(host='0.0.0.0',
                port=PORT,
                ssl_context=context,
                debug=DEBUG,
                # use_reloader=False
                )
        print 'Start Flask'
    except:
        print traceback.print_exc()
