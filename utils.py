#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pickle
from config import DB, COMMANDS_BOT, QUESTIONS_INIT, LANGUAGE_CALLBACK
import os


def getdb():
    try:
        with open(DB, 'rb') as f:
            data_new = pickle.load(f)
            return data_new
    except:
        return {}


def savedb(db_new):
    try:
        with open(DB, 'wb') as f:
            pickle.dump(db_new, f)
    except:
        return None
    return True


def checkdb(db):
    strerr = None
    status = True
    not_found = [i for i in COMMANDS_BOT if i not in db]
    if len(not_found) > 0:
        strerr = 'Bot: Not configured {}'.format(','.join(not_found))
        status = False
    return status, strerr


def check_dir(dirs):
    for i in dirs:
        client_path = os.path.join(os.path.dirname(__file__), i)
        if not os.path.exists(client_path):
            os.makedirs(client_path)


def set_symbol(s, pos=4, symbol='#'):
    try:
        if len(s) > 4:
            return s[:pos] + symbol + s[pos:]
        else:
            return s
    except:
        return s


if __name__ == '__main__':
    temp = '🎲qwerty'
    print len('🎲')
    print set_symbol(temp)
