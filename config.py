#!/usr/bin/env python
# -*- coding: utf-8 -*-


DEBUG = True

COMMANDS_BOT = ['moderator']
DB = 'db.pickle'

CLIENTTYPE = ['resume', 'vacancy']
LANGUAGE_CALLBACK = ['ru', 'en']
DEFAULT_LANGUAGE = 'ru'
THANKS_PHOTO = 'thanks_photo.jpg'
MODERATOR_ACTIVE = True

RESUME_ATTR = ['issolo', 'name', 'team_name', 'count_members', 'country_city', 'genre', 'specialization',
               'during_contract', 'salary', 'photo', 'video', 'check', 'edit',
               'add_about', 'about', 'end']

QUESTION_RESUME = {
    'order': RESUME_ATTR,
    'issolo': {
        'msg_type': 'menu',
        'ru': {
            'question': 'Отлично👌! Вы работаете сольно или в коллективе?',
            'variants': ['👤Соло', '👥Коллектив'],
        },
        'en': {
            'question': "Great👌! Do you work as a soloist or in team?",
            'variants': ['👤Solo', '👥Team'],
        },
        'next': {'0': 'name', '1': 'team_name'}
    },
    'name': {
        'msg_type': 'text',
        'ru': {
            'question': 'Очень хорошо👏! Напишите, пожалуйста, как Вас зовут👇!',
        },
        'en': {
            'question': "Very good👏! Write please your first name and last name👇",
        },
        'next': 'country_city'
    },
    'team_name': {
        'msg_type': 'text',
        'ru': {
            'question': 'Очень хорошо👏! Напишите, пожалуйста, название Вашего коллектива👇!',
        },
        'en': {
            'question': "Very well👏! Write please the name of your team👇!",
        },
        'next': 'country_city'
    },
    'count_members': {
        'msg_type': 'text',
        'ru': {
            'question': 'Сколько участников в Вашем коллективе? (от – до)',
        },
        'en': {
            'question': 'How many members in Your collective? (from - to)',
        },
        'next': 'country_city'
    },
    'country_city': {
        'msg_type': 'text',
        'ru': {
            'question': 'Рад познакомиться🤗! Теперь напишите страну и город🏙, в котором Вы проживаете'
                        ' (например «Украина, Киев»)',
        },
        'en': {
            'question': 'Nice to meet you🤗! Now write your country and the city🏙 (for example  "Ukraine, Kyiv")'
        },
        'next': 'genre'
    },
    'genre': {
        'msg_type': 'menu',
        'ncolumns': 1,
        'ru': {
            'question': 'Привет землякам😁, я тоже оттуда😉! Теперь выберите жанр, в котором Вы работаете👇',
            'variants': ['🎼Музыкант', '👠Танцор', '🎪Цирковой артист', '👔Персонал для отеля',
                         '🎭Смешанный коллектив', '🎗Другой'],
        },
        'en': {
            'question': "Hey bro😁, this is my motherland too😉! Now choose your genre👇",
            'variants': ['🎼Musician', '👠Dancer', '🎪Circus artist', '👔Hotel staff',
                         '🎭Mixed team', '🎗️Other'],
        },
        'next': 'specialization'
    },
    'specialization': {
        'msg_type': 'text',
        'ru': {
            'question': 'Невероятно😋, оказывается мы еще и коллеги🤝! Теперь напишите Вашу специализацию 🎓('
                         'например «Шоу балет», «Акробат» или «Официант»)',
        },
        'en': {
            'question': 'Unbelivable!!!😋, we are colleagues 🤝! Congratulations, you have chose right profession💪!'
                        '  Now write it here, please 🎓(for example  «Show ballet», «Acrobat» or «Waiter»)',
        },
        'next': 'during_contract'
    },
    'during_contract': {
        'msg_type': 'menu',
        'ru': {
            'question': '📆 Какую продолжительность контракта Вы рассматриваете?',
            'variants': ['🕐До 1 месяца', '🕑От 1 до 3 месяцев', '🕓От 3 до 6 месяцев', '🕧От 6 месяцев'],
        },
        'en': {
            'question': "📆 How long are you going to be working?",
            'variants': ['🕐 Till 1 month', '🕑 From 1 to 3 months', '🕓 From 3 to 6 months', '🕧 From 6 months']
        },
        'next': 'salary'
    },
    'salary': {
        'msg_type': 'text',
        'ru': {
            'question': 'Хорошо👌, напишите желаемый гонорар (в $$$) за 1 месяц работы на 1 человека💰',
        },
        'en': {
            'question': 'Nice👌, write your expective salary ($$$) per 1 month for 1 person💰.',
        },
        'next': 'photo'
    },
    'photo': {
        'msg_type': 'photo',
        'ru': {
            'question': 'Отлично👏! А теперь прикрепите 1 Ваше лучшее фото📸.',
        },
        'en': {
            'question': 'Exellent👏! Now send your the best photo📸',
        },
        'next': 'video'
    },
    'video': {
        'msg_type': 'text',
        'ru': {
            'question': 'Очень хорошее фото😊! Сейчас прикрепите ссылку на Ваше лучшее промо - видео'
                         '(не требуется для отельного персонала) 🎥.',
        },
        'en': {
            'question': 'Nice pic😊!Now attach the link on your best promo-video  (not required for hotel staff) 🎥',
        },
        'next': 'phone'
    },
    'phone': {
        'msg_type': 'phone',
        'ru': {
            'question': '📲Подтвердите свой контактный номер для связи с Вами👇',
            'text': '👌Подтверждаю!'
        },
        'en': {
            'question': '📲Confirm your contact number to keep in touch👇.',
            'text': '👌Confirm!'
        },
        'next': 'check'
    },
    'check': {
        'msg_type': 'menu',
        'ncolumns': 1,
        'ru': {
            'question': 'Остался последний шаг⌛️! Проверьте еще раз всю информацию, все верно? 👇',
            'variants': ['✅Да, все верно', '⛔️Нет, хочу изменить'],
            'text': '🤔Все верно?'
        },
        'en': {
            'question': "All right? Check again information about You",
            'variants': ['✅Yes, everything is right', '⛔️No, I need to change it'],
            'text': '🤔Is it right?'
        },
        'next': {'0': 'add_about', '1': 'edit'}
    },
    'add_about': {
        'msg_type': 'menu',
        'ru': {
            'question': 'Готово! Хотите еще что-то добавить📎',
            'variants': ['📎Да', '📍Нет'],
        },
        'en': {
            'question': "Done! Do you want to add anything else📎?",
            'variants': ['📎Yes', '📍No'],
        },
        'next': {'0': 'about', '1': 'end'}
    },
    'edit': {
        'msg_type': 'menu',
        'ru': {
            'question': 'Без проблем. Выберите, что Вы хотите изменить👇',
            'variants': ['👉Имя', '🕦Продолжнительность работы', '🏠Страна/город проживания', '🎲Жанр', '🎓Специализация',
                         '💰Гонорар', '📸Фото', '🎥Видео', '✅Все верно'],
        },
        'en': {
            'question': "No problem. Choose, what exactly you want to change👇",
            'variants': ['👉Name', '🕦Duration of work', '🏠Living country/city', '🎲Genre', '🎓Profession',
                         '💰Salary', '📸Photo', '🎥Video', '✅Everything is right'],
        },
        'edited': ['name', 'during_contract', 'country_city', 'genre',
                   'specialization', 'salary', 'photo', 'video', 'add_about'],

        'next': {'0': 'name', '1': 'during_contract', '2': 'country_city', '3': 'specialization',
                 '4': 'salary', '5': 'photo', '6': 'video', '7': 'add_about'}
    },
    'about': {
        'msg_type': 'text',
        'ru': {
            'question': 'Хорошо👍! Напишите дополнительную информацию в свободной форме🗒',
        },
        'en': {
            'question': 'Good👍! Write additional information in free form🗒',
        },
        'next': 'end'
    },
    'end': {
        'msg_type': 'text',
        'ru': {
            'question': ['Спасибо🙏, мы разместим всю информацию в ближайшее время🕐 \n\n🔔 А сейчас обязательно'
                         ' подпишитесь на наш канал и узнайте "7️⃣ САМЫХ ГРУБЫХ ОШИБОК ПРИ ПОИСКЕ РАБОТЫ ЗА ГРАНИЦЕЙ",'
                         ' а также другие лайфхаки 🕶 и секреты трудоустройства t.me/sbaworld  🏁\n\n👇Также следите'
                         ' за нами в соц. сетях: \n-  instagram.com/sba_world\n-  facebook.com/groups/sba.world\n\n'
                         '🌈Хорошего дня и будьте успешными вместе с нами!🏆 И ещё добавим одно сообщение,'
                         ' самое последнее. ]',

                         '\n❗️Чтобы создать резюме или вакансию ещё раз, нажмите 👉 /start'],
        },
        'en': {
            'question': ['Thank you🙏, we will post all your information very soon🕐\n\n🔔 Attention! Definitely follow'
                         ' our chanel, and you will now "7️⃣ THE MOST COMMON MISTAKES WHEN YOU ARE LOOKING FOR A'
                         ' CONTRACT", and also another life-hacks 🕶 and the secrets of employment -  t.me/sbaworld'
                         '  🏁\n\n👇Also follow us:\n-  instagram.com/sba_world\n-  facebook.com/groups/sba.world\n\n'
                         '🌈Have a nice day and be succesfull with us!🏆',

                         '\n❗️For creating resume and vacancy again - click  👉 /start'],
        },
        'next': 'end'
    },

}

VACANCY_ATTR = ['vacancy_name', 'country_city', 'job_place', 'job_place_name', 'name', 'genre', 'during_contract',
                'begin_date', 'salary', 'business_hours', 'employee_requirements', 'free_days_count', 'photo',
                'video', 'check', 'edit', 'add_about', 'about', 'end']

QUESTION_VACANCY = {
    'order': VACANCY_ATTR,
    'vacancy_name': {
        'msg_type': 'text',
        'ru': {
            'question': 'Отлично👌! Напишите, пожалуйста, название вакансии (например «Шоу балет», «Вокалистка»,'
                         ' «Аниматор» или «Акробат»)',
        },
        'en': {
            'question': 'Great👌! Write please name of the vacancy (For example, "Show ballet", "Singer", "Animator",'
                        ' "Acrobat")',
        },
        'next': 'country_city'
    },
    'country_city': {
        'msg_type': 'text',
        'ru': {
            'question': 'Хорошо🙏. Теперь напишите страну и город🏙места работы (например «ОАЭ, Дубай»)',
        },
        'en': {
            'question': 'Good! 🙏. Write please country and city 🏙 of work (for example "UAE, Dubai")',
        },
        'next': 'job_place'
    },
    'job_place': {
        'msg_type': 'menu',
        'ncolumns': 1,
        'ru': {
            'question': 'Очень хорошо👏! Вы предлагаете работу в …👇?',
            'variants': ['🛎Отель', '🎙Ночной клуб', '🎢Парк развлечений', '🎭Театр', '🍽Ресторан/бар',
                         '🛍Торговый центр', '🎪Цирк', '🛳Круизный лайнер', '🎉Ивент агентство', '🎗️Другой'],
        },
        'en': {
            'question': 'Very good👏! You offer the job in the …👇?',
            'variants': ['🛎Hotel', '🎙Night club', '🎢Amusement park', '🎭Theatre', '🍽Restaurant/Bar',
                         '🛍Shopping mall', '🎪Circus', '🛳Cruise ship', '🎉Event agency', '🎗️Other'],
        },
        'next': 'job_place_name'
    },
    'job_place_name': {
        'msg_type': 'text',
        'ru': {
            'question': 'Напишите ещё название места работы (например «Hilton» или «Successful Booking Agency»👇!',
        },
        'en': {
            'question': "Write please the name of working place  (for example «Hilton» или «Circus Du Solei»👇",
        },
        'next': 'name'
    },
    'name': {
        'msg_type': 'text',
        'ru': {
            'question': 'Отлично👍! Теперь напишите, пожалуйста,  как Вас зовут (Имя и Фамилия)👇?',
        },
        'en': {
            'question': "Great👍! Now write please your First name and Last name 👇",
        },
        'next': 'genre'
    },
    'genre': {
        'msg_type': 'menu',
        'ncolumns': 1,
        'ru': {
            'question': 'Рад познакомиться 🤗! Теперь выберите жанр работников, которые Вам нужны👇',
            'variants': ['🎼Музыканты', '👠Танцоры', '🎪Цирковые артисты', '👔Персонал для отеля',
                         '🎭Смешанные коллективы', '🎗️Другие'],
        },
        'en': {
            'question': 'Nice to meet you 🤗! Now you can choose working genre of your supposed employee 👇',
            'variants': ['🎼Musicians', '👠Dancers', '🎪Circus artists', '👔Hotel staff', '🎭Mixed teams',
                         '🎗️Other'],
        },
        'next': 'during_contract'
    },
    'during_contract': {
        'msg_type': 'menu',
        'ru': {
            'question': '📆 Какая продолжительность Вашего контракта?',
            'variants': ['🕐До 1 месяца', '🕑От 1 до 3 месяцев', '🕓От 3 до 6 месяцев', '🕧От 6 месяцев'],
        },
        'en': {
            'question': '📆What is the duration of your contract?',
            'variants': ['🕐 Till 1 month', '🕑 From 1 to 3 months', '🕓 From 3 to 6 months', '🕧 From 6 months']
        },
        'next': 'begin_date'
    },
    'begin_date': {
        'msg_type': 'text',
        'ru': {
            'question': 'Хорошо👌, напишите, пожалуйста, когда начало работы 📆 (например: 01.01.2018).',
        },
        'en': {
            'question': 'Very good👌, write please when the contract starts 📆 (for example: 01.01.2018).',
        },
        'next': 'salary'
    },
    'salary': {
        'msg_type': 'text',
        'ru': {
            'question': 'Напишите какая заработная плата 💰за 1 месяц работы для 1 человека (в $$$). ',
        },
        'en': {
            'question': "Write please the salary 💰per 1 month for 1 person💰",
        },
        'next': 'business_hours'
    },
    'business_hours': {
        'msg_type': 'text',
        'ru': {
            'question': 'А какой график работы🕗?',
        },
        'en': {
            'question': 'What about working schedule 🕗',
        },
        'next': 'employee_requirements'
    },
    'employee_requirements': {
        'msg_type': 'text',
        'ru': {
            'question': 'Хорошо👌! Осталось несколько вопросов. Напишите требования к работнику'
                         '(📏рост, 📰гражданство, 📈профессиональные навыки)',
        },
        'en': {
            'question': 'Well done👌! It remains just a couple of questions. Write the requirements for employee'
                        ' (📏height, 📰nationality, 📈',
        },
        'next': 'free_days_count'
    },
    'free_days_count': {
        'msg_type': 'text',
        'ru': {
            'question': 'Сколько выходных в месяц📅?',
        },
        'en': {
            'question': 'How many days off per month📅?',
        },
        'next': 'photo'
    },
    'photo': {
        'msg_type': 'photo',
        'ru': {
            'question': 'Отлично👏! А теперь прикрепите 1 лучшее фото места работы📸.',
        },
        'en': {
            'question': 'Exellent👏! Now please send the best photo of working place📸.',
        },
        'next': 'video'
    },
    'video': {
        'msg_type': 'text',
        'ru': {
            'question': 'Очень хорошее фото😊! Также прикрепите ссылки на видео 🎞с места работы или пример'
                         ' выступления необходимых артистов📹 (не обязательно).',
        },
        'en': {
            'question': 'Nice pic😊! Also send video links  🎞 from the working place or example of required artists📹 '
                        '(not necessary).',
        },
        'next': 'phone'
    },
    'phone': {
        'msg_type': 'phone',
        'ru': {
            'question': '📲Подтвердите свой контактный номер для связи с Вами👇',
            'text': '👌Подтверждаю!'
        },
        'en': {
            'question': '📲Confirm your contact number to keep in touch👇.',
            'text': '👌Confirm!'
        },
        'next': 'check'
    },
    'check': {
        'msg_type': 'menu',
        'ru': {
            'question': 'Остался последний шаг⌛️! Проверьте еще раз всю информацию, все верно? 👇',
            'variants': ['✅Да, все верно', '⛔️Нет, хочу изменить'],
            'text': '🤔Все верно?'
        },
        'en': {
            'question': "Last step⌛️! Please, check the all information you have written, is everything right? 👇",
            'variants': ['✅Yes, everything is right', '⛔️No, I need to change it'],
            'text': '🤔Is it right?'
        },
        'next': {'0': 'add_about', '1': 'edit'}
    },
    'add_about': {
        'msg_type': 'menu',
        'ru': {
            'question': 'Готово! Хотите еще что-то добавить📎',
            'variants': ['📎Да', '📍Нет'],
        },
        'en': {
            'question': "Done! Do you want to add anything else📎?",
            'variants': ['📎Yes', '📍No'],
        },
        'next': {'0': 'about', '1': 'end'}
    },
    'edit': {
        'msg_type': 'menu',
        'ncolumns': 1,
        'ru': {
            'question': 'Без проблем. Выберите, что Вы хотите изменить👇',
            'variants': ['👉Название вакансии', '👴Ваше Имя', '🏠Страна/город', '🎲Жанр работников', '📍Тип места работы',
                         '📌Название места работы', '🕦Продолжнительность работы', '📆Начало работы', '🗓График работы',
                         '💰Зарплата', '💤Количество выходных', '📸Фото', '🎥Видео ', '✅Все верно'],
        },
        'en': {
            'question': "No problem. Choose, what exactly you want to change👇",
            'variants': ['👉Name of the vacancy', '👴Your name', '🏠City/Country', '🎲Genre', '📍Tipe of working place',
                         '📌Nme of working place', '🕦Duration of contract', '📆Start of contract', '🗓Schedule',
                         '💰Salary', '💤Days off per month',  '📸Photo', '🎥Video ', '✅Все верно'],
        },
        'edited': ['vacancy_name', 'name', 'country_city', 'genre', 'job_place', 'job_place_name', 'during_contract',
                   'begin_date', 'business_hours', 'salary', 'free_days_count',   'photo', 'video',
                   'add_about'],

        'next': {'0': 'vacancy_name', '1': 'name', '2': 'country_city', '3': 'job_place', '4': 'job_place_name',
                 '5': 'during_contract', '6': 'begin_date', '7': 'business_hours', '8': 'salary',
                 '9': 'free_days_count', '10': 'genre', '11': 'photo', '12': 'video', '13': 'add_about'}
    },
    'about': {
        'msg_type': 'text',
        'ru': {
            'question': 'Хорошо👍! Напишите дополнительную информацию в свободной форме🗒',
        },
        'en': {
            'question': 'Good👍! Write additional information in free form🗒',
        },
        'next': 'end'
    },
    'end': {
        'msg_type': 'text',
        'ru': {
            'question': ['Спасибо🙏, мы разместим всю информацию в ближайшее время🕐 \n\n🔔 А сейчас обязательно'
                         ' подпишитесь на наш канал и узнайте "7️⃣ САМЫХ ГРУБЫХ ОШИБОК ПРИ ПОИСКЕ РАБОТЫ ЗА ГРАНИЦЕЙ",'
                         ' а также другие лайфхаки 🕶 и секреты трудоустройства t.me/sbaworld  🏁\n\n👇Также следите'
                         ' за нами в соц. сетях: \n-  instagram.com/sba_world\n-  facebook.com/groups/sba.world\n\n'
                         '🌈Хорошего дня и будьте успешными вместе с нами!🏆 И ещё добавим одно сообщение,'
                         ' самое последнее.',

                         '\n❗️️️Чтобы создать резюме или вакансию ещё раз, нажмите 👉 /start ']
        },
        'en': {
            'question': ['Thank you🙏, we will post all your information very soon🕐\n\n🔔 Attention! Definitely follow'
                         ' our chanel, and you will now "7️⃣ THE MOST COMMON MISTAKES WHEN YOU ARE LOOKING FOR A'
                         ' CONTRACT", and also another life-hacks 🕶 and the secrets of employment -  t.me/sbaworld'
                         '  🏁\n\n👇Also follow us:\n-  instagram.com/sba_world\n-  facebook.com/groups/sba.world\n\n'
                         '🌈Have a nice day and be succesfull with us!🏆',

                         '\n❗️For creating resume and vacancy again - click  👉 /start'],
        },
        'next': 'end'
    },

}

QUESTIONS_INIT = {
    'order': ['language', 'type'],
    'language': {
        'msg_type': 'menu',
        'ru': {
            'question': 'Привет👋. Я Ваш успешный помощник Артистико🕺. Сначала выберите язык!',
            'variants': ['🇷🇺Русский', '🇬🇧English'],
        },
        'en': {
            'question': "Hey👋. I am your succefull assistent Artistico 🕺. Firstly choose the language!",
            'variants': ['🇷🇺Русский', '🇬🇧English'],
        },
        'next': 'type'
    },
    'type': {
        'msg_type': 'menu',
        'ncolumns': 1,
        'ru': {
            'question': '💫Я помогу Вам разместить всю необходимую информацию!👇',
            'variants': ['👶Создать резюме', '👴Создать вакансию'],
        },
        'en': {
            'question': "💫 I will help you to put all the necessary information!👇",
            'variants': ['👶Create resume', '👴Create vacancy'],
        },
        'next': {'0': 0, '1': 1}
    }
}

RESUME_TEMPALTE = {
    'ru': '@Artistico_bot\n\n🔴#РЕЗЮМЕ\n\n👉 {8}: {0}\n\n🏠Место проживания: #{1}\n🎲Жанр: {2}\n🕦Ищу контракт: {3}'
          '\n💰Желаемый гонорар: {4} $\n🎥Видео: {5}\n✍️О себе: {6}\n💬Телеграм: {7}',
    # 'ru': '@Artistico_bot\n\n🔴#РЕЗЮМЕ\n\n👉 \n\n🏠Место проживания: #\n🎲Жанр: \n🕦Ищу контракт: '
    #       '\n💰Желаемый гонорар:  $\n🎥Видео: \n✍️О себе: \n💬Телеграм: ',

    'en': '@Artistico_bot\n\n🔴#Resume\n\n👉 {8}: {0}\n\n🏠Country/city: #{1}\n🎲Genre: {2}\n🕦Looking for contract: {3}'
          '\n💰Expective salary: {4} $\n🎥Promo: {5}\n✍️About mе: {6}\n💬Telegram: {7}',
}

RESUME_TEMPLATE_COLLECTIVE = {
    'ru': '@Artistico_bot\n\n🔴#РЕЗЮМЕ\n\n👉 {8}: {0}\n\n🏠Место проживания: #{1}\n🎲Жанр: {2}\n🕦Ищу контракт: {3}'
          '\n💰Желаемый гонорар: {4} $\n🎥Видео: {5}\n✍️О себе: {6}\n💬Телеграм: {7}',

    'en': '@Artistico_bot\n\n🔴#Resume\n\n👉 {8}: {0}\n\n🏠Country/city: #{1}\n🎲Genre: {2}\n🕦Looking for contract: {3}'
          '\n💰Expective salary: {4} $\n🎥Promo: {5}\n✍️About mе: {6}\n💬Telegram: {7}',

}

VACANCY_TEMPLATE = {
    'ru': '@Artistico_bot\n\n🔵#ВАКАНСИЯ\n\n👉{0}\n\n🎲Требуется: {1}\n📊Требования: {2}\n🏠 Страна/город: #{3}\n📍{4}: {5}'
          '\n👴Работодатель: {6}\n📆Начало: {7}\n🕦 Продолжительность: {8}\n💰 Зарплата: {9}$\n🗓График работы: {10}\n'
          '💤Количество выходных в месяц: {11}\n🎥 Видео: {12}\n✍️ Дополнительная информация: {13}\n💬 Телеграм:{14}',

    'en': '@Artistico_bot\n\n🔵#VACANCY \n\n👉{0}\n\n🎲Requiqured: {1}\n📊Requirements: {2}\n🏠 Country/city: #{3}'
          '\n📍{4}: {5}\n👴Employer: {6}\n📆Star: {7}\n🕦 Duration: {8}\n💰 Salary: {9}$\n🗓Working schedule: {10}\n'
          '💤Days off per month: {11}\n🎥 Video: {12}\n✍️  Additional information: {13}\n💬 Telegram:{14}',

}
