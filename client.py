#!/usr/bin/env python
# -*- coding: latin-1 -*-

from config import CLIENTTYPE, RESUME_ATTR, QUESTIONS_INIT, QUESTION_RESUME,\
    QUESTION_VACANCY, RESUME_TEMPALTE, RESUME_TEMPLATE_COLLECTIVE, VACANCY_ATTR, VACANCY_TEMPLATE, \
    DEFAULT_LANGUAGE
import os
import logging
import traceback
import pickle
from telegram import InlineKeyboardButton, InlineKeyboardMarkup, KeyboardButton, ReplyKeyboardMarkup
from utils import check_dir, set_symbol

logging.basicConfig(filename='client.log', format='%(asctime)s %(levelname)-8s %(message)s', level=logging.DEBUG,
                    datefmt='%Y-%m-%d %H:%M:%S')

check_dir(['clients'])
client_path = os.path.join(os.path.dirname(__file__), 'clients')


class Client(object):
    types = CLIENTTYPE

    def __init__(self, tid, is_new=False, username=None):
        self.path_id = os.path.join(client_path, str(tid))
        if not is_new and os.path.isfile(self.path_id):
            print 'load client'
            self.load()
        else:
            print 'create client'
            self.id = str(tid)
            self.language = DEFAULT_LANGUAGE
            self.current = QUESTIONS_INIT['order'][0]
            self.type_ = None
            self.config = QUESTIONS_INIT
            self.isfull = None
            self.end = None
            self.phone = None
            self.username = '@{}'.format(username.strip()) if username is not None else ''

    def __setattr__(self, name, value):
        object.__setattr__(self, name, value)
        if name != 'path_id' and self.id is not None:
            self.save()
        # print name, value

    def set_type(self, type_):
        self.type_ = 'resume' if type_ == '0' else 'vacancy'
        type_appl = RESUME_ATTR if type_ == '0' else VACANCY_ATTR

        for i in type_appl:
            self.__dict__[i] = None
        self.type_ = self.types[int(type_)]

    def save(self):
        # print vars(self)
        # path = os.path.join(client_path, str(self.id))
        logging.info('Start save client {}'.format(self.path_id))

        try:
            with open(self.path_id, 'wb') as f:
                    pickle.dump(vars(self), f)
        except:
            print logging.error(traceback.print_exc())
            logging.error(traceback.print_exc())
        logging.info('End save client {}'.format(self.path_id))
        return True

    def load(self):
        try:
            print self.path_id
            with open(self.path_id, 'rb') as f:
                data_new = pickle.load(f)
                for k, v in data_new.iteritems():
                    self.__dict__[k] = v
                # print 'data_new', data_new
                # return data_new
        except:
            logging.error(traceback.print_exc())

    def menu(self, bot=None, msg_id=None,  n_cols=2):
        print 'menu_current ',  self.current

        msg_type = self.config[self.current]['msg_type']
        text = self.config[self.current][self.language]['question']
        if msg_type == 'menu':
            if self.current == 'check':
                bot.sendMessage(text=text, chat_id=msg_id)
                resume_or_job = self.get_info

                bot.sendMessage(text=resume_or_job,  chat_id=msg_id)
                # print self.photo

                bot.send_photo(chat_id=msg_id, photo=self.photo)
                text = self.config[self.current][self.language]['text']

            # print 'menu_text', text, self.current
            variants = self.config[self.current][self.language]['variants']
            keyboart = []

            columnss = self.config[self.current]['ncolumns'] \
                if 'ncolumns' in self.config[self.current] else n_cols

            for k in range(0, len(variants), columnss):
                test = [(i, j) for i, j in enumerate(variants) if k <= i]
                row = [InlineKeyboardButton(j, callback_data=i) for i, j in enumerate(variants)
                       if k <= i < k + columnss]
                print 'row', test
                keyboart.append(row)

            reply_markup = InlineKeyboardMarkup(keyboart)
            return msg_type, text, reply_markup
        if msg_type in ['text', 'photo']:
            return msg_type, text
        if msg_type == 'phone':
            # button_text = self.config[self.current][self.language]['text']
            # contact_keyboard = KeyboardButton(text=button_text, request_contact=True)
            # custom_keyboard = [[contact_keyboard]]
            # reply_markup = ReplyKeyboardMarkup(custom_keyboard, one_time_keyboard=True, resize_keyboard=True)
            # bot.send_message(chat_id=msg_id,
            #                  text=text,
            #                  reply_markup=reply_markup)
            self.send_request_contact(bot, text, msg_id)
            return msg_type

    def go(self, value):
        if self.current == 'phone' and len(self.username) == 0 and value is None:
            return
        # print 'go_current ', self.current, value
        if self.current == 'check':
            self.isfull = True

        if self.current == 'language':
            print 'set language'
            self.language = 'ru' if value == '0' else 'en'

        elif self.current == 'type':
            self.config = QUESTION_RESUME if value == '0' else QUESTION_VACANCY
            self.set_type(value)
            self.current = self.config['order'][0]
            return

        else:
            if self.current != 'photo':
                self.__dict__[self.current] = value
        next = self.config[self.current]['next']
        if self.isfull and self.current not in ['check', 'add_about']:
            if self.current == 'edit':
                self.current = self.config['edit']['edited'][int(value)]
            else:
                self.current = 'check'

        else:
            if type(next) is dict:
                self.current = self.config[self.current]['next'][value]
            else:
                self.current = self.config[self.current]['next']
            if self.current == 'phone' and len(self.username) > 0:
                self.current = self.config[self.current]['next']

    def save_photo(self, message):
        if len(message.photo) > 2:
            self.photo = message.photo[2].file_id
        print 'save_photo end', self.photo
        return self.photo

    def send_request_contact(self, bot, text, msg_id):
        button_text = self.config[self.current][self.language]['text']
        contact_keyboard = KeyboardButton(text=button_text, request_contact=True)
        custom_keyboard = [[contact_keyboard]]
        reply_markup = ReplyKeyboardMarkup(custom_keyboard, one_time_keyboard=True, resize_keyboard=True)
        bot.send_message(chat_id=msg_id,
                         text=text,
                         reply_markup=reply_markup)
        self.__dict__['request_contact_active'] = True

    @property
    def get_current_type(self):
        return self.config[self.current]['msg_type']

    @property
    def isfull_and_check(self):
        return True if self.isfull and self.current == 'check' else False

    @property
    def is_end(self):
        return self.current == 'end'

    @property
    def get_info(self):
        resume_or_job = None
        def xstr(s):
            return '' if s is None else s.encode('utf8')
        genre = set_symbol((self.config['genre'][self.language]['variants'][int(self.genre)]))
        during_contract = self.config['during_contract'][self.language]['variants'][int(self.during_contract)] \
            if self.during_contract is not None else ''
        # resume
        tele_name = unicode(self.username) if len(self.username) > 0 else self.phone

        if self.type_ == 'resume':
            if self.issolo == '0':
                resume_or_job = RESUME_TEMPALTE[self.language].format(xstr(self.name), xstr(self.country_city),
                                                                      genre, during_contract, xstr(self.salary),
                                                                      xstr(self.video),
                                                                      xstr(self.about),
                                                                      tele_name,
                                                                      xstr(self.specialization),
                                                                      )
            else:
                resume_or_job = RESUME_TEMPLATE_COLLECTIVE[self.language].format(xstr(self.team_name),
                                                                                 xstr(self.country_city),
                                                                                 genre, during_contract,
                                                                                 xstr(self.salary),
                                                                                 xstr(self.video),
                                                                                 xstr(self.about),
                                                                                 tele_name,
                                                                                 xstr(self.specialization),
                                                                                 # xstr(self.count_members),
                                                                                 )
        # vacancy
        else:
            job_place = self.config['job_place'][self.language]['variants'][int(self.job_place)] \
                if self.job_place is not None else ''

            resume_or_job = VACANCY_TEMPLATE[self.language].format(xstr(self.vacancy_name),
                                                                   genre,
                                                                   xstr(self.employee_requirements),
                                                                   xstr(self.country_city),
                                                                   job_place,
                                                                   xstr(self.job_place_name),
                                                                   xstr(self.name),
                                                                   xstr(self.begin_date),
                                                                   during_contract,
                                                                   xstr(self.salary),
                                                                   xstr(self.business_hours),
                                                                   xstr(self.free_days_count),
                                                                   xstr(self.video),
                                                                   xstr(self.about),
                                                                   tele_name,
                                                                   )
        return resume_or_job

    @property
    def current_text(self):
        return self.config[self.current][self.language]['text']

    @property
    def current_question(self):
        return self.config[self.current][self.language]['question']

if __name__ == '__main__':
    a = Client('0')
    a.type_ = 0
    a.save_photo('qwe12313')
    a.load()
    # a.save_client()
    print vars(a)

    # b =Base()
    # b.a =5
    # print a
