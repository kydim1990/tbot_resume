# Self-signed SSL certificate (make sure 'Common Name' matches your FQDN):
openssl req -new -x509 -nodes -newkey rsa:1024 -keyout server.key -out server.crt -days 3650
# You can test SSL handshake running this script and trying to connect using wget:
# $ wget -O /dev/null https://$HOST:$PORT/

ps xau | grep telegram

/home/dkuzin/telegrambot/env/bin/gunicorn  --workers 3  --bind 0.0.0.0:8443 --certfile=server.crt --keyfile=server.key wsgi:app