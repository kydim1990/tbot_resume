from main_bot import app, setwebhook, context
from config import DEBUG
from config_server import PORT
import time

if __name__ == "__main__":
    setwebhook()
    time.sleep(5)
    app.run(host='0.0.0.0',
            port=PORT,
            ssl_context=context,
            debug=DEBUG)
